module Main where

import qualified Data.Map    as Map
import           Text.Parsec

type OrbitObject = String
type OrbitMap = Map.Map OrbitObject OrbitObject

addToMap :: OrbitMap -> OrbitObject -> OrbitObject -> OrbitMap
addToMap m o1 o2 = Map.insert o2 o1 m

pInput :: Parsec String OrbitMap OrbitMap
pInput = do
  _ <- many pLine
  eof
  getState

pLine :: Parsec String OrbitMap (OrbitObject, OrbitObject)
pLine = do
  o1 <- pOrbitObject
  _ <- char ')'
  o2 <- pOrbitObject
  _ <- char '\n'
  s <- getState
  let s' = addToMap s o1 o2
  putState s'
  return (o1,o2)

pOrbitObject :: Parsec String OrbitMap OrbitObject
pOrbitObject = many alphaNum

getParent :: OrbitMap -> OrbitObject -> Maybe OrbitObject
getParent m o = Map.lookup o m

getParentCount :: OrbitMap -> OrbitObject -> Int
getParentCount m o = case getParent m o of
  Nothing     -> 0
  Just parent -> 1 + getParentCount m parent


getParents :: OrbitMap -> OrbitObject -> [OrbitObject]
getParents m o  = case getParent m o of
  Nothing     -> []
  Just parent -> parent:getParents m parent

findFirstIntersect :: [String] -> [String] -> Maybe String
findFirstIntersect [] _ = Nothing
findFirstIntersect _ [] = Nothing
findFirstIntersect (x:xs) ys = if x `elem` ys
  then Just x
  else findFirstIntersect xs ys

distanceToElem :: [String] -> String -> Int
distanceToElem (x:xs) y = if y == x
  then 0
  else 1 + distanceToElem xs y

main :: IO ()
main = do
  --- input
  contents <- getContents
  let input = runParser pInput Map.empty "" contents
  case input of
    Right orbitMap -> do
      print orbitMap
      let keys = Map.keys orbitMap
      let parentCounts = map (getParentCount orbitMap) keys
      print $ "Keys: " ++ show keys
      print $ "Parent count: " ++ show parentCounts
      print $ "Problem 1: " ++ show (sum parentCounts)
      let youParents = getParents orbitMap "YOU"
      let sanParents = getParents orbitMap "SAN"
      print $ "YOU parents: " ++ show youParents
      print $ "SAN parents: " ++ show sanParents
      print $ "First common parent: " ++ case findFirstIntersect youParents sanParents of
        Nothing          -> "Has no common orbit"
        Just commonOrbit -> "Problem 2: " ++ show (youDistance + sanDistance)
          where youDistance = distanceToElem youParents commonOrbit
                sanDistance = distanceToElem sanParents commonOrbit

    Left err       -> putStrLn ("Failed to parse: " ++ show err)

