module Main where

import           Control.Arrow
import           Data.Array

type Mem = Array Int Int

set :: Mem -> Int -> Int -> Mem
set m i v = m // [(i,v)]

add :: Mem -> Int -> Int -> Int -> Mem
add mem i1 i2 i3 = set mem (mem!i3) (mem!(mem!i1) + mem!(mem!i2))

multiply :: Mem -> Int -> Int -> Int -> Mem
multiply mem i1 i2 i3 = set mem (mem!i3) (mem!(mem!i1) * mem!(mem!i2))

getArray :: [Int] -> Mem
getArray ls = listArray (0,length ls - 1) ls

run :: Int -> Mem -> Mem
run i m = case m!i of
  99 -> m
  1  -> run (i + 4) (add m (i+1) (i+2) (i+3))
  2  -> run (i + 4) (multiply m (i+1) (i+2) (i+3))
  v  -> error $ "invalid case value: " ++ show v

getOutput :: Int -> Int -> Mem -> Int
getOutput noun verb m = run 0 (set (set m 1 noun) 2 verb) ! 0

readInput :: String -> [Int]
readInput = map replaceComma >>> words >>> map read
  where replaceComma ',' = ' '
        replaceComma c   = c

main :: IO ()
main =  interact $
  readInput >>> getArray >>> replicate 2 >>> zipWith ($) [solve1, solve2] >>> show
    where solve1 = getOutput 12 2
          solve2 mem = head [ 100 * n + v | n <- [0..99], v <- [0..99], getOutput n v mem == 19690720]
