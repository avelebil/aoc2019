module Main where

import           Control.Arrow
import           Data.Array
import           Data.List

type Mem = Array Int Int

set :: Mem -> Int -> Int -> Mem
set m i v = m // [(i,v)]

add :: Mem -> Int -> Int -> Int -> Mem
add mem i1 i2 i3 = set mem i3 (i1 + i2)

multiply :: Mem -> Int -> Int -> Int -> Mem
multiply mem i1 i2 i3 = set mem i3 (i1 * i2)

getArray :: [Int] -> Mem
getArray ls = listArray (0,length ls - 1) ls

step :: Int -> (Mem, [Int], [Int]) -> (Mem,[Int],[Int])
step i (m,input,output) = case opcode of
  99 -> (m,input,output)
  1  -> step (i + 4) (add m p1val p2val p3val,input,output)
  2  -> step (i + 4) (multiply m p1val p2val p3val,input,output)
  3  -> case input of
    []       -> error "Unexpected missing input"
    inp:inps -> step (i + 2) (set m (m!(i+1)) inp, inps, output)
  4  -> step (i + 2) (m, input, (m!(m!(i+1))):output)
  5  -> case p1val of
    0 -> step  (i + 3) (m, input, output)
    _ -> step p2val (m, input, output)
  6  -> case p1val of
    0 -> step p2val (m, input, output)
    _ -> step  (i + 3) (m, input, output)
  7 -> if p1val < p2val then step (i + 4) (set m p3val 1, input, output)
       else step (i + 4) (set m p3val 0, input, output)
  8 -> if p1val == p2val then step (i + 4) (set m p3val 1, input, output)
       else step (i + 4) (set m p3val 0, input, output)
  v  -> error $ "invalid case value: " ++ show v
  where opc = m!i
        opcode = opc `mod` 100
        p1val
          | (opc `div` 100) `mod` 10 == 0 = m!(m!(i+1))
          | otherwise = m!(i+1)
        p2val
          | (opc `div` 1000 ) `mod` 10 == 0 = m!(m!(i+2))
          | otherwise = m!(i+2)
        p3val = m!(i+3)

run :: [Int] -> Mem -> Int
run input m = let (_,_,output) = step 0 (m,input,[]) in
  head output

readInput :: String -> [Int]
readInput = map replaceComma >>> words >>> map read
  where replaceComma ',' = ' '
        replaceComma c   = c

solve :: String -> String
solve s =
  show $ maximum [ try (pa,pb,pc,pd,pe) | [pa, pb, pc, pd, pe] <- permutations [0,1,2,3,4] ]
  where try :: (Int, Int, Int, Int, Int) -> Int
        try (pa,pb,pc,pd,pe) = let [amp_a, amp_b, amp_c, amp_d, amp_e] = replicate 5 $ getArray $ readInput s
                                   ra = run [pa, 0] amp_a
                                   rb = run [pb, ra] amp_b
                                   rc = run [pc, rb] amp_c
                                   rd = run [pd, rc] amp_d
                                   re = run [pe, rd] amp_e
                               in re

main :: IO ()
main =  interact solve
