module Main where

import           Control.Arrow

main :: IO ()
main = interact solve

verifyFact :: Int -> Bool
verifyFact p = (hasAdjacents sp || startsWithDouble sp) && onlyIncreases sp
  where sp = show p
        hasAdjacents :: String -> Bool
        hasAdjacents [] = False
        hasAdjacents [_] = False
        hasAdjacents [_,_] = False
        hasAdjacents [i,j,k]
          | j == k && i /= j = True
          | otherwise = False
        hasAdjacents (i:j:k:l:xs)
          | j == k && i /= j && l /= k = True
          | otherwise = hasAdjacents (j:k:l:xs)
        startsWithDouble :: String -> Bool
        startsWithDouble (x:y:z:_)
          | x == y && x /= z = True
          | otherwise = False
        startsWithDouble _ = False
        onlyIncreases :: String -> Bool
        onlyIncreases [] = True
        onlyIncreases [_] = True
        onlyIncreases (x:xs)
          | head xs < x = False
          | otherwise = onlyIncreases xs


solve :: String -> String
solve = words >>> map read >>> (\ls -> [head ls .. ls!!1]) >>> filter verifyFact >>> length >>>  show
