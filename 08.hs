module Main where

import           Control.Arrow

type Layer = String

readLayers :: Int -> Int -> String -> [Layer]
readLayers _ _ [] = []
readLayers w h xs = take area xs:readLayers w h (drop area xs)
  where area = w * h

getLayerRows :: Int -> Layer -> [String]
getLayerRows _ [] = []
getLayerRows w xs = take w xs : getLayerRows w (drop w xs)

countSymbols :: (Int, Int, Int) -> String -> (Int, Int, Int)
countSymbols values [] = values
countSymbols (zeroes, ones, twos) ('0':xs) = countSymbols (zeroes + 1, ones, twos) xs
countSymbols (zeroes, ones, twos) ('1':xs) = countSymbols (zeroes, ones + 1, twos) xs
countSymbols (zeroes, ones, twos) ('2':xs) = countSymbols (zeroes, ones, twos + 1) xs
countSymbols values (_:xs) = countSymbols values xs

solve1 :: [Layer] -> Int
solve1 = map (countSymbols (0,0,0)) >>> minimum >>> (\(_,ones,twos) -> ones * twos)

solve2 :: [Layer] -> Layer
solve2 = foldl1 combineLayers
  where combineLayers :: Layer -> Layer -> Layer
        combineLayers [] _          = []
        combineLayers _ []          = []
        combineLayers [x] [y]       = [selectColor x y]
        combineLayers (x:xs) (y:ys) = selectColor x y:combineLayers xs ys
        selectColor :: Char -> Char -> Char
        selectColor '2' c = c
        selectColor c _   = c

printLayer :: Layer -> IO ()
printLayer l = mapM_ (putStrLn . map convertToSym) (getLayerRows 25 l)
  where convertToSym '0' = ' '
        convertToSym _   = '*'

main :: IO ()
main = do
  contents <- getContents
  let layers = (lines >>> map (readLayers 25 6)) contents
  putStrLn $ "Solution 1: " ++ show (solve1 $ head layers)
  putStrLn "Solution 2:"
  mapM_ (printLayer . solve2) layers

