module Main where

import           Control.Arrow
import           Data.Array

type Mem = Array Int Int

set :: Mem -> Int -> Int -> Mem
set m i v = m // [(i,v)]

add :: Mem -> Int -> Int -> Int -> Mem
add mem i1 i2 i3 = set mem i3 (i1 + i2)

multiply :: Mem -> Int -> Int -> Int -> Mem
multiply mem i1 i2 i3 = set mem i3 (i1 * i2)

getArray :: [Int] -> Mem
getArray ls = listArray (0,length ls - 1) ls

withIO :: [Int] -> [Int] -> Mem -> (Mem, [Int], [Int])
withIO input output mem = (mem, input, output)

run :: Int -> (Mem, [Int], [Int]) -> (Mem,[Int],[Int])
run i (m,input,output) = case opcode of
  99 -> (m,input,output)
  1  -> run (i + 4) (add m p1val p2val p3val,input,output)
  2  -> run (i + 4) (multiply m p1val p2val p3val,input,output)
  3  -> case input of
    []       -> error "Unexpected missing input"
    inp:inps -> run (i + 2) (set m (m!(i+1)) inp, inps, output)
  4  -> run (i + 2) (m, input, (m!(m!(i+1))):output)
  5  -> case p1val of
    0 -> run  (i + 3) (m, input, output)
    _ -> run p2val (m, input, output)
  6  -> case p1val of
    0 -> run p2val (m, input, output)
    _ -> run  (i + 3) (m, input, output)
  7 -> if p1val < p2val then run (i + 4) (set m p3val 1, input, output)
       else run (i + 4) (set m p3val 0, input, output)
  8 -> if p1val == p2val then run (i + 4) (set m p3val 1, input, output)
       else run (i + 4) (set m p3val 0, input, output)
  v  -> error $ "invalid case value: " ++ show v
  where opc = m!i
        opcode = opc `mod` 100
        p1val
          | (opc `div` 100) `mod` 10 == 0 = m!(m!(i+1))
          | otherwise = m!(i+1)
        p2val
          | (opc `div` 1000 ) `mod` 10 == 0 = m!(m!(i+2))
          | otherwise = m!(i+2)
        p3val = m!(i+3)

solve :: (Mem,[Int],[Int]) -> (Mem,[Int],[Int])
solve (m,input,output) = run 0 (m, input, output)

readInput :: String -> [Int]
readInput = map replaceComma >>> words >>> map read
  where replaceComma ',' = ' '
        replaceComma c   = c

main :: IO ()
main =  interact $
  readInput >>> getArray >>> withIO [5] [] >>> solve >>> show
