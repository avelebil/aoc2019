module Main where

import           Control.Arrow

type Point = (Int, Int)
type Line = (Point, Point)
type Wire = [Line]
type WireState = (Point, [Line])

data LineS a = U a
             | D a
             | L a
             | R a
  deriving Show

type WireR = [LineS Int]


inInterval :: Int -> [Int] -> Bool
inInterval i interval = i >= mini && i <= maxi
  where mini = minimum interval
        maxi = maximum interval

buildWireR :: [String] -> WireR
buildWireR [] = []
buildWireR ((f:val):xs)
  | f == 'R' = R value : buildWireR xs
  | f == 'L' = L value : buildWireR xs
  | f == 'D' = D value : buildWireR xs
  | f == 'U' = U value : buildWireR xs
  | otherwise = error "Failed to parse input"
  where value = read val :: Int

type Segment = (Int, Point, LineS Int)

convertToSegments :: Int -> Point -> WireR -> [Segment]
convertToSegments _ _ [] = []
convertToSegments s p (l:ls) = (s, p, l) : convertToSegments (addSteps s l) (addLineS p l) ls

addLineS :: Point -> LineS Int -> Point
addLineS (x,y) (R value) = (x+value,y)
addLineS (x,y) (L value) = (x-value,y)
addLineS (x,y) (U value) = (x,y+value)
addLineS (x,y) (D value) = (x,y-value)

addSteps :: Int -> LineS Int -> Int
addSteps v (R value) = v+value
addSteps v (L value) = v+value
addSteps v (U value) = v+value
addSteps v (D value) = v+value

-- input is segment of wire 1 and segment of wire 2
-- output is intersection point and steps for wire 1 and wire 2
intersectionS :: Segment -> Segment -> (Point, Int, Int)
intersectionS (s1,(x11,y11),l1) (s2,(x21,y21),l2)
  | x11 == x12 && y21 == y22 && inInterval x11 [x21, x22] && inInterval y21 [y11, y12] = ((x11, y21), r1, r2)
  | x21 == x22 && y11 == y12 && inInterval x21 [x11, x12] && inInterval y11 [y21, y22] = ((x21, y11), r1, r2)
  | otherwise = ((0,0),0,0)
    where x12 = case l1 of
            L val -> x11 - val
            R val -> x11 + val
            _     -> x11
          y12 = case l1 of
            U val -> y11 + val
            D val -> y11 - val
            _     -> y11
          x22 = case l2 of
            L val -> x21 - val
            R val -> x21 + val
            _     -> x21
          y22 = case l2 of
            U val -> y21 + val
            D val -> y21 - val
            _     -> y21
          r1 = case l1 of
            R _ -> s1 + (x21 - x11)
            L _ -> s1 + (x11 - x21)
            U _ -> s1 + (y21 - y11)
            D _ -> s1 + (y11 - y21)
          r2 = case l2 of
            R _ -> s2 + (x11 - x21)
            L _ -> s2 + (x21 - x11)
            U _ -> s2 + (y11 - y21)
            D _ -> s2 + (y21 - y11)


solve2 :: [[Segment]] -> Int
solve2 [w1,w2] = minimum [ r1+r2 | (p, r1, r2) <- [ intersectionS s1 s2 | s1 <- w1, s2 <- w2], p /= (0,0) ]
solve2 _       = error "Unexpected input"

main :: IO ()
main = interact $
   lines >>> map (map replaceComma >>> words >>> buildWireR >>> convertToSegments 0 (0,0)) >>> solve2 >>> (\r -> show r ++ "\n")
  where replaceComma ',' = ' '
        replaceComma c   = c
