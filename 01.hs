module Main where

import           Control.Arrow
import qualified Data.ByteString.Lazy.Char8 as C
import           Data.Maybe                 (fromJust)

main :: IO ()
main = C.interact $
  C.lines >>> map readInt >>> map computeFuel >>> sum >>> (\r -> show r ++ "\n") >>> C.pack

readInt :: C.ByteString -> Int
readInt = C.readInt >>> fromJust >>> fst

computeFuel :: Int -> Int
computeFuel mass = if mass > 6 then fuel + computeFuel fuel
                   else 0
  where fuel = mass `div` 3 - 2
